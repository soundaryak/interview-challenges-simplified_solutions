/**
 * Test 1
 * This is the first coding challenge. Below are two arrays. 
 * The second array is identical to the first except for one number.
 * Find the missing number and log it in the console
 *
 * You may use any development tools or package manager you like. 
 * Lodash is included as a bower.json dependency in case you would like to use it.
 */
// Number 8 is missing, now we can able to see missing number in console

var arr = [1,2,5,7,4,8,12,15,17,37];
var secondArr = [1,17,5,12,4,7,15,2,37];
var missing_number = arr.filter(function(x) {
            return !secondArr.includes(x);
        });
console.log(missing_number);


/*
Solution Using Jquery

var diff = $(arr).not(secondArr).get();

*/
